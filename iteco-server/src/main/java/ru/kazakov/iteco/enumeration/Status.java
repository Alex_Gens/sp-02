package ru.kazakov.iteco.enumeration;

import org.jetbrains.annotations.NotNull;

public enum Status {

    PLANNED("Planned"),
    IN_PROGRESS("In progress"),
    READY("Ready");

    @NotNull
    private String displayName;

    private Status(@NotNull final String displayName) {this.displayName = displayName;}

    @NotNull
    public String getDisplayName() {return this.displayName;}

}
