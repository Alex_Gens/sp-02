package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.Nullable;
import java.util.List;

public interface IService<T> {

    public void remove(@Nullable final String id) throws Exception;

    public void removeAll() throws Exception;

    @Nullable
    public T findOne(@Nullable final String id) throws Exception;

    @Nullable
    public List<T> findAll() throws Exception;

}
