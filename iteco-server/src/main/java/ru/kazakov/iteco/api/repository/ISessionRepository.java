package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import ru.kazakov.iteco.entity.Session;

public interface ISessionRepository extends CrudRepository<Session, String> {

    public void deleteSessionByUserId(String userId);

    public boolean existsByUserIdAndId(
            @NotNull final String userId,
            @NotNull final String id
    );

}
