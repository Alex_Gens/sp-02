package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.enumeration.SortType;
import java.util.List;

public interface IProjectRepository extends PagingAndSortingRepository<Project, String> {

      public void deleteAllProjectsByUserId(@NotNull final String currentUserId);

    @Nullable
    public Project findProjectByUserIdAndName(
            @NotNull final String currentUserId,
            @NotNull final String name);

    @NotNull
    public List<Project> findAllProjectsByUserId(@NotNull final String currentUserId);

    @NotNull
    public List<Project> findAllProjectsByUserId(
            @NotNull final String currentUserId,
            @NotNull final Sort sort
    );

    @NotNull
    public List<Project> findAllByUserIdAndNameLike(
            @NotNull final String currentUserId,
            @NotNull final String part
    );

    @NotNull
    public List<Project> findAllByUserIdAndInfoLike(
            @NotNull final String currentUserId,
            @NotNull final String part);

    public boolean existsByName(@NotNull final String name);

    public boolean existsByUserIdAndName(
            @NotNull final String currentUserId,
            @NotNull final String name);

    @Query("select case when (count(p.id) > 0) then false else true end from Project p where p.user.id = ?1")
    public boolean isEmptyProjectRepository(@NotNull final String currentUserId);

}