package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import java.util.List;

public interface ITaskRepository extends PagingAndSortingRepository<Task, String> {

    public void deleteAllTasksByUserId(@NotNull final String currentUserId);

    @Nullable
    public Task findTaskByUserIdAndName(
            @NotNull final String currentUserId,
            @NotNull final String name
    );

    @NotNull
    public List<Task> findAllTasksByUserId(@NotNull final String currentUserId);

    @NotNull
    public List<Task> findAllTasksByUserId(
            @NotNull final String currentUserId,
            @NotNull final Sort sort
    );

    @NotNull
    public List<Task> findAllTasksByUserIdAndProjectId(
            @NotNull final String currentUserId,
            @NotNull final String projectId,
            @NotNull final Sort sort
    );

    @NotNull
    public List<Task> findAllTasksByUserIdAndNameLike(
            @NotNull final String currentUserId,
            @NotNull final String part);

    @NotNull
    public List<Task> findAllTasksByUserIdAndInfoLike(
            @NotNull final String currentUserId,
            @NotNull final String part);

    public boolean existsByName(@NotNull final String name);

    public boolean existsByUserIdAndName(
            @NotNull final String currentUserId,
            @NotNull final String name
    );

    @Query("select case when (count(t.id) > 0) then false else true end from Task t where t.user.id = ?1")
    public boolean isEmptyTaskRepository(@NotNull final String currentUserId);

}
