package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import ru.kazakov.iteco.entity.User;

public interface IUserRepository extends CrudRepository<User, String> {

    @Nullable
    public User findUserByLogin(@NotNull final String login);

    public boolean existsByLogin(@NotNull final String login);

}
