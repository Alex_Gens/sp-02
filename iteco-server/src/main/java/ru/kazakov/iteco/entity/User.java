package ru.kazakov.iteco.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.enumeration.RoleType;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "users")
public class User extends AbstractEntity  {

    @Column
    @Nullable
    private String name;

    @Column(unique = true)
    @Nullable
    private String login;

    @Column
    @Nullable
    private String password;

    @Column(name = "date_create")
    @NotNull
    private Date dateCreate = new Date();

    @Column(name = "date_start")
    @Nullable
    private Date dateStart;

    @Column(name = "date_finish")
    @Nullable
    private Date dateFinish;

    @Column(name = "role_type")
    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.DEFAULT;

    @Nullable
    @OneToMany(mappedBy = "user")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Collection<Project> projects = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Collection<Task> tasks = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Collection<Session> sessions = new ArrayList<>();

}
