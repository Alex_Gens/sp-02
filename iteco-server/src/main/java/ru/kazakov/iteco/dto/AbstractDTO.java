package ru.kazakov.iteco.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property= "ru/kazakov/iteco/dto")
@JsonSubTypes({
        @JsonSubTypes.Type(value= ProjectDTO.class, name="project"),
        @JsonSubTypes.Type(value= TaskDTO.class, name="task"),
        @JsonSubTypes.Type(value= UserDTO.class, name="user")})
public abstract class AbstractDTO implements Serializable {

    public static final long serialVersionUID = 1L;

    @NotNull
    protected String id = UUID.randomUUID().toString();

}
