package ru.kazakov.iteco.service;

import com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.kazakov.iteco.api.repository.ITaskRepository;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TaskService extends AbstractService<Task> implements ITaskService {
    
    @NotNull
    @Autowired
    private ITaskRepository repository;

    @Override
    public void persist(@Nullable final Task entity) throws Exception {
        if (entity == null) throw new Exception();
        if (repository.existsById(entity.getId())) throw new Exception();
        repository.save(entity);
        
    }

    @Override
    public void merge(@Nullable final Task entity) throws Exception {
        if (entity == null) throw new Exception();
        repository.save(entity);

    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        repository.deleteById(id);
        
    }

    @Override
    public void create(@Nullable final Session session,
                       @Nullable final String name
    ) throws Exception {
        if (session == null) throw new Exception();
        if (name == null || name.isEmpty()) throw new Exception();
        @NotNull final Task task = new Task();
        task.setUser(session.getUser());
        task.setName(name);
        persist(task);
    }

    @Override
    public void removeAll() throws Exception {
        repository.deleteAll();
    }

    @Override
    public void removeAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        repository.deleteAllTasksByUserId(currentUserId);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Task task = repository.findById(id).orElse(null);
        return task;
    }

    @Nullable
    @Override
    public Task findByName(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Task task = repository.findTaskByUserIdAndName(currentUserId, name);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @Nullable final List<Task> list = Lists.newArrayList(repository.findAll());
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final List<Task> list = repository.findAllTasksByUserId(currentUserId);
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAll(
            @Nullable final String currentUserId,
            @Nullable final SortType sortType
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        @NotNull final Sort sort;
        switch (sortType) {
            case START : sort = Sort.by("dateStart"); break;
            case FINISH: sort = Sort.by("dateFinish"); break;
            case STATUS: sort = Sort.by("status"); break;
            default    : sort = Sort.by("dateCreate"); break;
        }
        @NotNull final List<Task> list = repository.findAllTasksByUserId(currentUserId, sort);
        return list == null ? Collections.emptyList()
                            : list.stream().map(Task::getName).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<String> findAll(
            @Nullable final String currentUserId,
            @Nullable final String projectId,
            @Nullable final SortType sortType) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        @NotNull final Sort sort;
        switch (sortType) {
            case START : sort = Sort.by("dateStart"); break;
            case FINISH: sort = Sort.by("dateFinish"); break;
            case STATUS: sort = Sort.by("status"); break;
            default    : sort = Sort.by("dateCreate"); break;
        }
        @NotNull final List<Task> list = repository.findAllTasksByUserIdAndProjectId(currentUserId, projectId, sort);
        return list == null ? Collections.emptyList()
                            : list.stream().map(Task::getName).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<String> findAllByName(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final List<Task> list =
                repository.findAllTasksByUserIdAndNameLike(currentUserId, "%" + part + "%");
        return list == null ? Collections.emptyList()
                            : list.stream().map(Task::getName).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<String> findAllByInfo(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final List<Task> list =
                repository.findAllTasksByUserIdAndInfoLike(currentUserId, "%" + part + "%");
        return list == null ? Collections.emptyList()
                            : list.stream().map(Task::getName).collect(Collectors.toList());
    }

    @Override
    public boolean contains(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        final boolean contains = repository.existsByName(name);
        return contains;
    }

    @Override
    public boolean contains(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        final boolean contains = repository.existsByUserIdAndName(currentUserId, name);
        return contains;
    }

    @Override
    public boolean isEmptyRepository(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        final boolean isEmpty = repository.isEmptyTaskRepository(currentUserId);
        return isEmpty;
    }

}
