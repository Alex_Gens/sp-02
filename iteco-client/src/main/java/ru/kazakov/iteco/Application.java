package ru.kazakov.iteco;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import ru.kazakov.iteco.context.Bootstrap;

@ComponentScan
public class Application {

    public static void main(String[] args) {
        ApplicationContext context =
                new AnnotationConfigApplicationContext(Application.class);
        @NotNull final Bootstrap bootstrap = context.getBean("bootstrap", Bootstrap.class);
        bootstrap.init();
    }

}