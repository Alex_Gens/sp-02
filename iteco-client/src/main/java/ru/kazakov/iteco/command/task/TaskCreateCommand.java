package ru.kazakov.iteco.command.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public final class TaskCreateCommand extends TaskAbstractCommand {

    @Getter
    @NotNull
    private final String name = "task-create";

    @Getter
    @NotNull
    private final String description = "Create new task.";

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (taskEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        terminalService.write("ENTER TASK NAME: ");
        @NotNull final String name = terminalService.enterIgnoreEmpty().trim();
        if (taskEndpoint.containsTaskByCurrentId(token, name)) {
            terminalService.write("[NOT CREATED]");
            terminalService.write("Task with this name is already exists. Use another task name.");
            terminalService.separateLines();
            return;
        }
        taskEndpoint.createTask(token, name);
        terminalService.write("[CREATED]");
        terminalService.write("Task successfully created!");
        terminalService.separateLines();
    }

}
