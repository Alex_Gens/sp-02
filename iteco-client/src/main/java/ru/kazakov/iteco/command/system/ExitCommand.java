package ru.kazakov.iteco.command.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class ExitCommand extends AbstractCommand {

    public final boolean secure = false;

    @Getter
    @NotNull
    private final String name = "exit";

    @Getter
    @NotNull
    private final String description = "Close task manager.";

    @NotNull
    private RoleType roleType = RoleType.NONE;

    @NotNull
    @Override
    public RoleType getRoleType() {return this.roleType;}

    @Override
    public void execute() throws Exception {
        if (terminalService == null) throw new Exception();
        terminalService.write("*** MANAGER CLOSED ***");
        System.exit(0);
    }

}
