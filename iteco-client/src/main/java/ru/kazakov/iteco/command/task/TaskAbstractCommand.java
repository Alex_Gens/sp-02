package ru.kazakov.iteco.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.ITaskEndpoint;
import ru.kazakov.iteco.api.endpoint.TaskDTO;
import ru.kazakov.iteco.command.AbstractCommand;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Component
@NoArgsConstructor
public abstract class TaskAbstractCommand  extends AbstractCommand {

    @Nullable
    @Autowired
    protected ITaskEndpoint taskEndpoint;

    @Nullable
    protected TaskDTO getTaskByPart() throws Exception {
        if (terminalService == null) throw new Exception();
        if (taskEndpoint == null) throw new Exception();
        if (currentState == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        terminalService.write("ENTER PART OF TASK NAME OR DESCRIPTION: ");
        @NotNull final String part = terminalService.enterIgnoreEmpty().trim();
        terminalService.separateLines();
        @Nullable final List<String> tasksByName = taskEndpoint.findAllTasksByNameCurrentId(token, part);
        @Nullable final List<String> tasksByInfo = taskEndpoint.findAllTasksByInfoCurrentId(token, part);
        if (tasksByName == null) throw new Exception();
        if (tasksByInfo == null) throw new Exception();
        if (tasksByName.isEmpty() && tasksByInfo.isEmpty()) {
            terminalService.write("Task doesn't exist. Use \"task-list\" to show all tasks.");
            terminalService.separateLines();
            return null;
        }
        int counter = 1;
        @NotNull final Map<String, String> tasksByNameMap = new TreeMap<>();
        @NotNull final Map<String, String> tasksByInfoMap = new TreeMap<>();
        for (String taskName : tasksByName) {
            tasksByNameMap.put(String.valueOf(counter), taskName);
            counter++;
        }
        for (String taskName : tasksByInfo) {
            tasksByInfoMap.put(String.valueOf(counter), taskName);
            counter++;
        }
        @Nullable final TaskDTO task;
        while (true) {
            if (!tasksByNameMap.isEmpty()) terminalService.write("Found by name: ");
            tasksByNameMap.forEach((k, v) -> terminalService.write(k + ". " + v));
            if (!tasksByInfoMap.isEmpty()) terminalService.write("Found by description:");
            tasksByInfoMap.forEach((k, v) -> terminalService.write(k + ". " + v));
            terminalService.separateLines();
            terminalService.write("Select task.");
            terminalService.write("ENTER TASK NUMBER: ");
            @NotNull final String number = terminalService.enterIgnoreEmpty();
            if (!tasksByNameMap.containsKey(number) && !tasksByInfoMap.containsKey(number)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("This number of item doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            if (tasksByNameMap.containsKey(number)) {
                @NotNull final String taskName = tasksByNameMap.get(number);
                task = taskEndpoint.findByTaskNameCurrentId(token, taskName);
                terminalService.write("[CORRECT]");
                terminalService.separateLines();
                break;
            }
            if (tasksByInfoMap.containsKey(number)) {
                @NotNull final String taskName = tasksByNameMap.get(number);
                task = taskEndpoint.findByTaskNameCurrentId(token, taskName);
                terminalService.write("[CORRECT]");
                terminalService.separateLines();
                break;
            }
        }
        return task;
    }

}
