package ru.kazakov.iteco.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.IDomainEndpoint;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.command.AbstractCommand;

@Component
@NoArgsConstructor
public abstract class DataAbstractLoadCommand extends AbstractCommand {

    @Nullable
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @NotNull
    protected final String directory = "data";

    @NotNull
    protected RoleType roleType = RoleType.ADMINISTRATOR;

    @NotNull
    @Override
    public RoleType getRoleType() {return this.roleType;}

    protected boolean confirmed() throws Exception {
        if (terminalService == null) throw new Exception();
        @NotNull String answer;
        while (true) {
            terminalService.write("All progress after last save will be lost and you will be logout. Continue?");
            terminalService.write("1. Yes");
            terminalService.write("2. No");
            terminalService.separateLines();
            terminalService.write("ENTER ANSWER NUMBER: ");
            answer = terminalService.enterIgnoreEmpty().trim();
            if (!answer.equals("1") && !answer.equals("2")) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("Enter correct number.");
                terminalService.separateLines();
                continue;
            }
            if (answer.equals("2")) {
                terminalService.write("[NOT LOADED]");
                terminalService.separateLines();
                return false;
            }
            break;
        }
        terminalService.separateLines();
        return true;
    }

}
