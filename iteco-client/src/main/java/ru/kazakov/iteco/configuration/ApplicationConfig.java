package ru.kazakov.iteco.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.kazakov.iteco.api.endpoint.*;
import ru.kazakov.iteco.endpoint.*;

@Configuration
public class ApplicationConfig {

    @Bean
    @NotNull
    public IProjectEndpoint projectEndpoint() {return new ProjectEndpointService().getProjectEndpointPort();}

    @Bean
    @NotNull
    public ITaskEndpoint taskEndpoint() {return new TaskEndpointService().getTaskEndpointPort();}

    @Bean
    @NotNull
    public IUserEndpoint userEndpoint() {return new UserEndpointService().getUserEndpointPort();}

    @Bean
    @NotNull
    public ISessionEndpoint sessionEndpoint() {return new SessionEndpointService().getSessionEndpointPort();}

    @Bean
    @NotNull
    public IDomainEndpoint domainEndpoint() {return new DomainEndpointService().getDomainEndpointPort();}

}
